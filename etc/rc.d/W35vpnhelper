#!/bin/sh

# prevent double startiptables configure in one time
while [ -e /tmp/vpnhelper_runing ]; do
    # Sleep until file does exists/is created
    usleep 500000
done
echo $$ > /tmp/vpnhelper_runing

# get params
. /etc/scripts/global.sh

# get local param always
eval `nvram_buf_get 2860 vpnInterface vpnUser vpnPassword l2tp_srv_enabled`

# constants
LOG="logger -t vpnhelper"
ppp=/etc/ppp

start() {
    if [ "$vpnEnabled" = "on" ]; then
      # wait connetc to ap in sta mode
      # if VPN=PPPOE and vpnInterface=LAN no need wait
      if [ "$vpnInterface" != "LAN" ] || [ "$vpnType" != "0" ]; then
    	    wait_connect
      fi

      $LOG "Start vpnhelper"
      # clear all configs and generate new
      echo > $ppp/connect-errors
      echo "$vpnUser * $vpnPassword *" > $ppp/chap-secrets
      echo "$vpnUser * $vpnPassword *" > $ppp/pap-secrets

      # call to vpn
      if [ "$vpnType" = "0" ]; then
	$LOG "PPPOE calling."
	(sleep 2 && /etc/scripts/config-pppoe.sh) &
      elif [ "$vpnType" = "1" ]; then
	$LOG "PPTP calling."
	(sleep 2 && /etc/scripts/config-pptp.sh) &
      elif [ "$vpnType" = "2" ]; then
	$LOG "L2TP calling."
	(sleep 2 && /etc/scripts/config-l2tp.sh) &
      elif [ "$vpnType" = "3" ]; then
	lanauthpid=`pidof lanauth`
	if [ "$lanauthpid" != "" ]; then
	    $LOG "LANAUTH call reload."
	    /etc/scripts/config-lanauth.sh reload &
	else
	    $LOG "LANAUTH calling."
	    (sleep 2 && /etc/scripts/config-lanauth.sh start) &
	fi
      fi
    fi
}

stop() {
    # Kill helpers
    killall -q config-pppoe.sh
    killall -q config-l2tp.sh
    killall -q config-pptp.sh
    killall -q config-lanauth.sh
    killall -q -SIGKILL config-pppoe.sh
    killall -q -SIGKILL config-l2tp.sh
    killall -q -SIGKILL config-pptp.sh
    killall -q -SIGKILL config-lanauth.sh

    # Kill lanauth only if vpn disable or not lanauth mode
    if [ "$vpnEnabled" = "off" ] || [ "$vpnType" != "3" ]; then
	/etc/scripts/config-lanauth.sh stop
    fi

    # close alll ppp based tuns and kill daemons
    xl2tpdpid=`pidof xl2tpd`
    pppdpid=`pidof pppd`
    if [ "$xl2tpdpid" != "" ] || [ "$pppdpid" != "" ]; then
	$LOG "Disconnect from vpn servers and stop daemons."
	# delete all routes to vpnnet
	# this prevent deadloop in kernel
	ip -4 route flush dev $vpn_if > /dev/null 2>&1

	# clear conntrack and routes tables/caches
	flush_net_caches

        # if xl2tp configured correct terminate xl2tpd daemon
	if [ -e /etc/ppp/l2tpd.conf ] && [ "$xl2tpdpid" != "" ]; then
	    # first disconnect
	    if [ -e /var/run/xl2tpd/l2tp-control ]; then
		lac=`cat /etc/ppp/l2tpd.conf | grep lns | cut -f 2- -d =`
		if [ "$lac" != "" ]; then
		    $LOG "xl2tpd: disconnect from $lac."
		    echo "d $lac" > /var/run/xl2tpd/l2tp-control
		    usleep 500000
		fi
	    fi
	    # second terminate xl2tpd client only
	    $LOG "xl2tpd: daemon terminate."
	    # terminate xl2tp daemon
	    while killall -q xl2tpd; do
		usleep 500000
		killall -q -SIGKILL xl2tpd
	    done
	fi

	# safe terminate pppd if l2tp server enabled
	if [ "$l2tp_srv_enabled" = "1" ] || [ "$MODEMENABLED" = "1" -a "$MODEMTYPE" != "2" ]; then
	    # first send HUP for terminate connections and try some times
	    # second send TERM for exit pppd process
	    # if process not terminated send KILL
	    # vpn client always use $vpn_if
    	    if [ -e /var/run/$vpn_if.pid ]; then
		pppdpid=`cat /var/run/$vpn_if.pid`
		if [ "$pppdpid" != "" ]; then
		    $LOG "pppd: close connection for pid $pppdpid."
		    kill -SIGHUP "$pppdpid"  > /dev/null 2>&1
		fi
		# terminate pppd
		count=0
		$LOG "pppd: daemon terminate $pppdpid."
		while kill "$pppdpid" > /dev/null 2>&1; do
		    if [ "$count" = "5" ]; then
			$LOG "pppd: daemon kill $pppdpid."
			kill -SIGKILL "$pppdpid"  > /dev/null 2>&1
			count=0
			usleep 500000
		    fi
		    count="$(($count+1))"
		    usleep 500000
		done
		rm -f /var/run/$vpn_if.pid
	    fi
	else # fast terminate ppp clients
	    # try disconnect before kill
	    killall -q -SIGHUP pppd
	    usleep 500000
	    # terminate pppd daeon
	    while killall -q pppd; do
		usleep 500000
		killall -q -SIGKILL pppd
	    done
	    # remove pid files
	    rm -f /var/run/ppp*.pid
	fi

	# remove VPN server IP file
	rm -f /tmp/vpnip
	# remove vpn_if_name export
	rm -f /tmp/vpn_if_name
	# remove client control socket
	rm -f /var/run/xl2tpd/l2tp-control
	# remove xl2tpd client pid
	rm -f /var/run/xl2tpd/l2tpd.pid
	# remove resolv.conf
	flush_net_caches
    fi
    rm -f /etc/ppp/resolv.conf
    # clear conntrack and routes tables/caches
}

case "$1" in
	start)
	    start
	    ;;

	stop)
	    stop
	    ;;

	stop_safe)
	    stop_safe
	    ;;

	restart)
	    stop
	    start
	    ;;

	*)
	    echo $"Usage: $0 {start|stop|stop_safe|restart}"
esac

# remove running flag
rm -f /tmp/vpnhelper_runing
