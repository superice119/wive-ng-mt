#!/bin/sh

# if app not exist
if [ ! -e /bin/igmpproxy ]; then
    exit 0
fi

# get params
. /etc/scripts/global.sh

LOG="logger -t igmpproxy"

start() {
    # if igmpproxy enable and mode not full bridge
    if [ "$igmpEnabled" = "1" ] && [ "$OperationMode" != "0" ] && [ "$ApCliBridgeOnly" != "1" ]; then
      # wait wireless connect if need
      wait_connect
      ( # send to background
	# check ip adress set at wan
	if [ "$wan_is_not_null" = "0" ]; then
    	    $LOG "Wait 15 sec before start."
	    sleep 15
	fi

	#generate config file
	get_param
	gen_config

	# disable arp filter for lan (temp fix iptv hybryd snooping work)
	if [ "$igmpSnoopMode" != "n" ]; then
    	    sysctl -wq net.ipv4.conf.$lan_if.arp_filter=0
	fi

	# if need kill igmpproxy
	igmp_is_start=`pidof igmpproxy`
	if [ "$igmp_is_start" != "" ]; then
	    $LOG "Stopping IGMPPROXY"
    	    # terminate igmpproxy daemon
    	    while killall -q igmpproxy; do
        	usleep 500000
        	killall -q -SIGKILL igmpproxy
    	    done
	fi

        $LOG "Starting IGMPPROXY"
    	igmpproxy $OPTS /etc/igmpproxy.conf &
      ) & # send to background
    fi
}

gen_config() {
####################generate config###########################
if [ "$igmpFastLeave" = "1" ]; then
    igmpFastLeave="quickleave"
else
    igmpFastLeave=""
fi

printf "
$igmpFastLeave
phyint $wan_if upstream ratelimit 0 threshold 1
altnet 0.0.0.0/0
phyint $lan_if downstream ratelimit 0 threshold 1
phyint $vpn_if disabled
" > /etc/igmpproxy.conf
##############################################################
}

get_param() {
    eval `nvram_buf_get 2860 igmpSnoopMode M2UEnabled igmpFastLeave VlanLan VlanWifiLan VlanWifiLanINIC VlanWifiWan VlanWifiWanINIC`
    ####################################
    # remap wan if to vlan bridge
    ####################################
    if [ "$tv_port" = "1" ] && [ "$tv_portVLAN" != "" ] && [ "$tv_port_mcast" = "1" ]; then
	wan_if="$vlantvif"
    fi
    if [ "$sip_port" = "1" ] && [ "$sip_portVLAN" != "" ] && [ "$sip_port_mcast" = "1" ]; then
	wan_if="$vlansipif"
    fi
    $LOG "Replace route to multicast subnet via $wan_if."
    ip -4 route replace "$mcast_net" dev "$wan_if"
    sysctl -wq net.ipv4.conf.$wan_if.forwarding=1

    ####################################
    # wan port number set
    ####################################
    if [ "$wan_port" = "4" ]; then
	# switch managment mode enabled
	igmpPort="w"
    fi
    ####################################
    # auto M2U enable in wifi drivers
    ####################################
    if [ "$M2UEnabled" = "1" ] && [ "$CONFIG_MT7610_AP_IGMP_SNOOP" != "" -o "$CONFIG_MT76X2_AP_IGMP_SNOOP" != "" -o "$CONFIG_MT76X3_AP_IGMP_SNOOP" != "" ]; then
	M2UEnabled="a"
    else
	M2UEnabled=""
    fi
    ####################################
    # correct mode for igmpsnooping
    ####################################
    if [ "$igmpSnoopMode" = "n" ] || [ "$igmpSnoopMode" = "h" ] || [ "$tv_port" = "1" ] || [ "$sip_port" = "1" ] || \
	[ "$VlanLan" != "" ] || [ "$VlanWifiLan" != "" ] || [ "$VlanWifiLanINIC" != "" ] || [ "$VlanWifiWan" != "" ] || [ "$VlanWifiWanINIC" != "" ]; then
	# if force HW by user or enabled portmapped switch config - hibryd mode snooping disable
	# some modes incompatable with snooping, need disable this, need use M2U for this modes
	igmpSnoopMode="n"
    else
        # default snooping auto mode enable
        igmpSnoopMode=""
    fi
    ####################################
    # gen options for daemon
    ####################################
    if [ "$igmpPort" != "" ] || [ "$igmpSnoopMode" != "" ] || [ "$M2UEnabled" != "" ]; then
	OPTS="-$igmpPort$igmpSnoopMode$M2UEnabled"
    else
	OPTS=""
    fi
}

stop() {
    pid=`pidof igmpproxy`
    if [ "$pid" != "" ]; then
	$LOG "Stopping IGMPPROXY"
	# terminate igmpproxy daemon
	while killall -q igmpproxy; do
	    usleep 500000
	    killall -q -SIGKILL igmpproxy
	done
    fi
}

case "$1" in
        start)
            start
            ;;

        stop)
            stop
            ;;

        restart)
            stop
            start
            ;;

        *)
            echo $"Usage: $0 {start|stop|restart}"
esac
