#!/bin/sh

LOG="logger -t vlan"

# get params
. /etc/scripts/global.sh

##########################################################################
# Incoming VLAN prio to SKB priomap (ingress)
# Outgoings SKB priomap to VLAN prio (egress)
# 0-7 vlan prio to 0-7 skb prio
##########################################################################
# in future add user must be set egress mapping
##########################################################################
set_default_vlan_prio_map()
{
    for vlannum in `seq 0 7`; do
	# vlan priority tag => skb->priority mapping
        vconfig set_ingress_map $1 $vlannum $vlannum
	# skb->priority => vlan priority tag mapping
        vconfig set_egress_map $1  $vlannum $vlannum
    done
}

set_egress_vlan_prio_map_tv()
{
    # get prio for needed vid
    if [ "$tv_portVLANPRIO" != "" ]; then
	qostag="$tv_portVLANPRIO"
    elif [ "$2" != "" ]; then
	qostag="$2"
    fi
    if [ "$qostag" != "" ]; then
	vconfig set_egress_map $1 0 $qostag
    fi
}

set_egress_vlan_prio_map_sip()
{
    # get prio for needed vid
    if [ "$sip_portVLANPRIO" != "" ]; then
	qostag="$sip_portVLANPRIO"
    elif [ "$2" != "" ]; then
	qostag="$2"
    fi
    if [ "$qostag" != "" ]; then
	vconfig set_egress_map $1 0 $qostag
    fi
}

##########################################################################
# set correct mode for arp processing, avoid collisions in arp cache
##########################################################################
set_arp_filter_mode()
{
    sysctl -wq net.ipv4.conf.$1.arp_announce=2
    sysctl -wq net.ipv4.conf.$1.arp_ignore=1
    sysctl -wq net.ipv4.conf.$1.arp_filter=1
    sysctl -wq net.ipv4.conf.$1.rp_filter=0
}

##########################################################################
# this stubs prevent arp announse jam
# need for correct bridge untag and tag segments
##########################################################################
set_mac_ip_vlan()
{
    # get oui from wan
    VENDMASK=`echo $WAN_MAC_ADDR | cut -f -3 -d :`
    # generate new last octets
    RNDMACLO=`printf "%02x:%02x:%02x" $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)) | awk '{print toupper($0) }'`
    # temp mac per vlan
    VLANMAC="$VENDMASK:$RNDMACLO"
    # temp not route ip to vlan
    VLANRIP="169.254.$((RANDOM%253+1)).$((RANDOM%253+1))"
    # set mac per vlan
    ifconfig "$1" hw ether "$VLANMAC" txqueuelen "$txqueuelen"
    # set ip per vlan
    # sikp sip/tv ports
    if [ "$2" != "skipip" ]; then
	ip -4 addr replace $VLANRIP/32 dev $1
    fi
}

##########################################################################
# MT7620 need set vpan mapping in raeth, depended from PPE offload mode
# calculate in set it
##########################################################################
idx=6
hw_tx_vlan_map()
{
    if [ -e /proc/mt7620/vlan_tx ]; then
	# VIDs < 6 not need map
	if [ "$1" -lt "6" ]; then
	    return
	fi
	# use slots 6..10 for custom VID
	# NOTE: slots 11..15 used by hw_nat WiFi/USB offload
	if [ "$offloadMode" = "2" ] || [ "$offloadMode" = "3" ]; then
	    maxslots="12"
	else
	    maxslots="16"
	fi
	if [ "$idx" = "$maxslots" ]; then
	    if [ "$vlanDoubleTag" != "1" ]; then
		$LOG "Overflow number of max supported vlans $maxslots, please enable vlan doble tag support for switch to software vlan_tx mode."
	    fi
	    return
	fi
	echo "$idx:$1" > /proc/mt7620/vlan_tx
	idx="$(($idx+1))"
    fi
}

bridgewlvlan() {
    count=0
    for vid in $1
    do
	if [ "$vid" != "0" ] && [ -d /proc/sys/net/ipv4/conf/$3${count} ]; then
	    # add and configure vlan bridges only one time
	    if [ ! -d /proc/sys/net/ipv4/conf/$2.$vid ]; then
		$LOG "Add VLAN interface $2.${vid} to system"
		vconfig add "$2" "${vid}"
		hw_tx_vlan_map "${vid}"
		set_default_vlan_prio_map "$2.${vid}"
		$LOG "Add BRIDGE brvl${vid}wl${count} to system"
		brctl addbr "brvl${vid}wl${count}"
		set_arp_filter_mode "brvl${vid}wl${count}"
		set_mac_ip_vlan "brvl${vid}wl${count}" "skipip"
		ip link set "brvl${vid}wl${count}" up
		$LOG "Add VLAN interface $2.${vid} to bridge brvl${vid}wl${count}"
		set_arp_filter_mode "$2.${vid}"
		set_mac_ip_vlan "$2.${vid}" "skipip"
		brctl addif "brvl${vid}wl${count}" "$2.${vid}"
		ip link set "$2.${vid}" up
	    fi
	    $LOG "Move wlan interface $3${count} from br0 and to new bridge brvl${vid}wl${count}"
	    # note: not touch mac on vlan interfaces
	    delif_from_br br0 "$3${count}"
	    brctl addif "brvl${vid}wl${count}" "$3${count}"
	    set_arp_filter_mode "$3${count}"
	    ip link set "$3${count}" up
	fi
	count="$(($count+1))"
    done
}

start() {
    get_param

    if [ ! -e /tmp/bootgood ]; then
	##############################################
	# ADD BRIDGES FOR VLAN PARTS USED
	# ADD VLAN LINKS FORM KERNEL TO ESW PORST
	# ONLY ONE TIME AT START!!!
	##############################################
	if [ "$tv_port" = "1" -a "$tv_portVLAN" != "" ] || [ "$sip_port" = "1" -a "$sip_portVLAN" != "" ]; then
	    # isolated LAN<=>VLAN ports allways configure at eth2
	    rootif="eth2"
	    if [ "$tv_port" = "1" ] && [ "$tv_portVLAN" != "" ]; then
		$LOG "Add bridge for TV/STB"
		brctl addbr "$vlantvif"
		set_arp_filter_mode "$vlantvif"
		set_mac_ip_vlan "$vlantvif"
		ip link set "$vlantvif" up
		$LOG "Add vlan to esw for TV/STB"
		vconfig add $rootif 3
		hw_tx_vlan_map "3"
		set_default_vlan_prio_map "${rootif}.3"
		set_arp_filter_mode "${rootif}.3"
		set_mac_ip_vlan "${rootif}.3" "skipip"
		brctl addif "$vlantvif" "${rootif}.3"
		ip link set "${rootif}.3" up
	    fi
	    if [ "$sip_port" = "1" ] && [ "$sip_portVLAN" != "" ]; then
		$LOG "Add bridge for SIP/STB"
		brctl addbr "$vlansipif"
		set_arp_filter_mode "$vlansipif"
		set_mac_ip_vlan "$vlantvif"
		ip link set "$vlansipif" up
		$LOG "Add vlan to esw for SIP/STB"
		vconfig add $rootif 4
		hw_tx_vlan_map "4"
		set_default_vlan_prio_map "${rootif}.4"
		set_arp_filter_mode "${rootif}.4"
		set_mac_ip_vlan "${rootif}.4" "skipip"
		brctl addif "$vlansipif" "${rootif}.4"
		ip link set "${rootif}.4" up
	    fi
	fi
    else
	##############################################
	# ADD WAN VLANS FOR IPTV/SIP/ETC USAGE
	##############################################
	if [ "$tv_port" = "1" ] && [ "$tv_portVLAN" != "" ]; then
	    needarpcacheclear=1
	    vidcount=0
	    for vid in $tv_portVLAN; do
		$LOG "Add vlan $vid for TV/STB port input (WAN) $tv_portVLAN"
		vidcount="$(($vidcount+1))"
		vconfig add "$phys_wan_if" "$vid"
		hw_tx_vlan_map "$vid"
		set_default_vlan_prio_map "${phys_wan_if}.${vid}"
		set_egress_vlan_prio_map_tv "${phys_wan_if}.${vid}" "$vidcount"
		set_arp_filter_mode "${phys_wan_if}.${vid}"
		set_mac_ip_vlan "${phys_wan_if}.${vid}" "skipip"
		brctl addif "$vlantvif" "${phys_wan_if}.${vid}"
		ip link set "${phys_wan_if}.${vid}" up
	    done
	fi
	if [ "$sip_port" = "1" ] && [ "$sip_portVLAN" != "" ]; then
	    needarpcacheclear=1
	    vidcount=0
	    for vid in $sip_portVLAN; do
		$LOG "Add vlan $vid for SIP port input (WAN) $sip_portVLAN"
		vidcount="$(($vidcount+1))"
		vconfig add "$phys_wan_if" "$vid"
		hw_tx_vlan_map "$vid"
		set_default_vlan_prio_map "${phys_wan_if}.${vid}"
		set_egress_vlan_prio_map_sip "${phys_wan_if}.${vid}" "$vidcount"
		set_arp_filter_mode "${phys_wan_if}.${vid}"
		set_mac_ip_vlan "${phys_wan_if}.${vid}" "skipip"
		brctl addif "$vlansipif" "${phys_wan_if}.${vid}"
		ip link set "${phys_wan_if}.${vid}" up
	    done
	fi
	#############################################
	# correct lan ifname for add vlans
	#############################################
	if [ "$switchpart" = "LLLLL" ]; then
	    phys_lan_if="eth2"
	fi
	#############################################
	# ADD LAN VLANS FOR ZONE ISOLATED NETWORKS
	#############################################
	VlanLan=`nvram_get 2860 VlanLan | awk '{ gsub(","," "); print }'`
	if [ "$VlanLan" != "" ] && [ "$VlanLan" != "0" ] && [ "$lan_if" = "br0" ]; then
	    needarpcacheclear=1
	    for vid in $VlanLan
	    do
		$LOG "Add LAN network VLAN interface ${phys_lan_if}.${vid} to system"
		vconfig add "$phys_lan_if" "$vid"
		hw_tx_vlan_map "$vid"
		set_default_vlan_prio_map "${phys_lan_if}.${vid}"
		set_arp_filter_mode "${phys_lan_if}.${vid}"
		set_mac_ip_vlan "${phys_lan_if}.${vid}"
		brctl addif "$lan_if" "${phys_lan_if}.${vid}"
		ip link set "${phys_lan_if}.${vid}" up
	    done
	    # in router mode allow isolate vlans
	    if [ "$switchpart" != "LLLLL" ]; then
		VlanLanIsolate=`nvram_get 2860 VlanLanIsolate | awk '{ gsub(","," "); print }'`
		for vid in $VlanLanIsolate
		do
		    $LOG "VLAN ${phys_lan_if}.${vid} isolate from others in bridge."
		    echo 1 > /sys/devices/virtual/net/${phys_lan_if}.${vid}/brport/isolate_mode
		done
	    else
		$LOG "AP mode need remove root interface from bridge to work vlan"
		brctl delif "$lan_if" "$phys_lan_if"
	    fi
	fi
	#############################################
	# ADD VLAN TO WLAN BRIDGES
	#############################################
	VlanWifiLan=`nvram_get 2860 VlanWifiLan | awk '{ gsub(","," "); print }'`
	if [ "$VlanWifiLan" != "" ] && [ "$VlanWifiLan" != "0" ] && [ "$lan_if" = "br0" ]; then
	    needarpcacheclear=1
	    bridgewlvlan "$VlanWifiLan" "${phys_lan_if}" "$first_wlan_mbss"
	fi
	VlanWifiLanINIC=`nvram_get 2860 VlanWifiLanINIC | awk '{ gsub(","," "); print }'`
	if [ "$VlanWifiLanINIC" != "" ] && [ "$VlanWifiLanINIC" != "0" ] && [ "$lan_if" = "br0" ]; then
	    needarpcacheclear=1
	    bridgewlvlan "$VlanWifiLanINIC" "${phys_lan_if}" "$second_wlan_mbss"
	fi
	if [ "$switchpart" != "LLLLL" ]; then
	    VlanWifiWan=`nvram_get 2860 VlanWifiWan | awk '{ gsub(","," "); print }'`
	    if [ "$VlanWifiWan" != "" ] && [ "$VlanWifiWan" != "0" ] && [ "$lan_if" = "br0" ]; then
		needarpcacheclear=1
		bridgewlvlan "$VlanWifiWan" "${phys_wan_if}" "$first_wlan_mbss"
	    fi
	    VlanWifiWanINIC=`nvram_get 2860 VlanWifiWanINIC | awk '{ gsub(","," "); print }'`
	    if [ "$VlanWifiWanINIC" != "" ] && [ "$VlanWifiWanINIC" != "0" ] && [ "$lan_if" = "br0" ]; then
		needarpcacheclear=1
		bridgewlvlan "$VlanWifiWanINIC" "${phys_wan_if}" "$second_wlan_mbss"
	    fi
	fi

	#############################################
	# ISOLATE UNTAGGED LAN INTERFACE FROM BIDGE
	#############################################
	if [ "$switchpart" != "LLLLL" ] && [ "$VlanUntagIsolate" = "1" ]; then
	    $LOG "Untagged LAN ${phys_lan_if} isolate from others in bridge."
	    echo 1 > /sys/devices/virtual/net/${phys_lan_if}/brport/isolate_mode
	fi

	#############################################
	# CALL USER SCRIPTS AFTER VLAN CONFIG
	#############################################
	if [ -d /etc/vlan.d -a -x /bin/run-parts ]; then
	    $LOG "Run scripts from /etc/vlan.d"
	    run-parts /etc/vlan.d
	fi

	#############################################
	# clear arp tables in ESW and kernel
	#############################################
	if [ "$needarpcacheclear" = "1" ]; then
	    flush_arp_caches
	fi
    fi
}

stop() {
    ##############################################
    # REMOVE WAN VLANS FOR IPTV/SIP/ETC USAGE
    ##############################################
    if [ "$CONFIG_RAETH_BOTH_GMAC" = "y" ]; then
	oldwanvlans=`ip -o -4 link show up | grep 'eth3\.' | awk {' print $2 '} | cut -f -1 -d @`
    else
	oldwanvlans=`ip -o -4 link show up | grep 'eth2.2\.' | awk {' print $2 '} | cut -f -1 -d @`
    fi
    for iface in $oldwanvlans
    do
	# remove tv/sip vlans network interfaces
	ip link set "$iface" down		> /dev/null 2>&1
	brctl delif "$vlantvif" "$iface"	> /dev/null 2>&1
	brctl delif "$vlansipif" "$iface"	> /dev/null 2>&1
	vconfig rem "$iface"			> /dev/null 2>&1
    done
    ##############################################
    # REMOVE LAN VLANS FOR ZONE ISOLATED NETWORKS
    ##############################################
    if [ "$switchpart" = "LLLLL" ] || [ "$CONFIG_RAETH_BOTH_GMAC" = "y" -a "$tv_portVLAN" = "" -a "$sip_portVLAN" = "" ]; then
	oldlanvlans=`ip -o -4 link show up | grep 'eth2\.' | awk {' print $2 '} | cut -f -1 -d @`
    else
	oldlanvlans=`ip -o -4 link show up | grep 'eth2.1\.' | awk {' print $2 '} | cut -f -1 -d @`
    fi
    for iface in $oldlanvlans
    do
	# remove isolated vlans network interfaces
	ip link set "$iface" down		> /dev/null 2>&1
	brctl delif "$lan_if" "$iface"		> /dev/null 2>&1
	vconfig rem "$iface"			> /dev/null 2>&1
    done
    ##############################################
    # REMOVE BRIDGE INTERFACES FOR VLAN BRIDGES
    ##############################################
    bridgenames=`brctl show | grep brvl | awk {' print $1 '}`
    for bridge in $bridgenames
    do
	for num in `seq 0 4`; do
	    brctl delif "$bridge" "ra$num"	> /dev/null 2>&1
	    brctl delif "$bridge" "rai$num"	> /dev/null 2>&1
	done
	ip link set "$bridge" down		> /dev/null 2>&1
	brctl delbr "$bridge"			> /dev/null 2>&1
    done
}

get_param() {
    # need get offlad mode for limit hw_vlan_tx slot use
    eval `nvram_buf_get 2860 vlanDoubleTag offloadMode VlanUntagIsolate`
    if [ -e /proc/sys/net/core/vlan_double_tag ]; then
	# get currecnt double vlan tag mode if present
	vlanDoubleTag=`cat /proc/sys/net/core/vlan_double_tag`
    fi
}

case "$1" in
	start)
	    start
	    ;;

	stop)
	    stop
	    ;;

	restart)
	    stop
	    start
	    ;;

	*)
	    echo $"Usage: $0 {start|stop|restart}"
esac
